import { Prefab, SpriteFrame, Node } from "cc";
import { Singleton } from "../common/base";
import { IState } from "../common/state";
import { ActorManager } from "../entity/actor/ActorManager";
import { JoyStickManager } from "../ui/JoyStickManager";

const ACTOR_SPEED = 120;

export default class DataManager extends Singleton {
  static get Instance() {
    return super.GetInstance<DataManager>();
  }
  prefabMap: Map<string, Prefab> = new Map();
  textureMap: Map<string, SpriteFrame[]> = new Map();

  actorMap: Map<number, ActorManager> = new Map();
  state: IState = {
    actors: [],
  };

  stage: Node;
  jm: JoyStickManager;
  account: string;

  applyState(state: IState) {
    this.state = state;
  }

  applyInput(data: any) {
    if (data.account === this.account) {
      return;
    }
    const actor = this.state.actors.find((e) => e.id === data.id);
    if (!actor) {
      return;
    }
    const { directionX, directionY, dt } = data;

    actor.posX += directionX * ACTOR_SPEED * dt;
    actor.posY += directionY * ACTOR_SPEED * dt;
  }

  reset() {
    this.state = {
      actors: [],
    };
    this.actorMap = new Map();
  }
}
